package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

public class Deck {
    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        if(size < 0){
         size = 1;
        }
        else if(size > 10){
            size = 10;
        }
        List<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < size; i++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    cards.add(new Card(rank, suit));
                }
            }
        }
        return cards;
    }

    public static int costOf(List<Card> hand) {
        int cost = 0;
        for(Card card : hand){
            cost += card.getCost();
        }
        return cost;
    }
}
