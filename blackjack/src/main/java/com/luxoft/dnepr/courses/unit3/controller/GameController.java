package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
    private static GameController controller;
    private List<Card> deck;
    private List<Card> myHand;
    private List<Card> dealersHand;
    private static final int BLACK_JACK = 21;
    private static final int DEALER_MAX = 17;

    private GameController() {
        deck = Deck.createDeck(1);
        myHand = new ArrayList<Card>();
        dealersHand = new ArrayList<Card>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        if (!myHand.isEmpty() || !dealersHand.isEmpty()) {
            reset();
        }

        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        shuffler.shuffle(deck);
        giveCard(myHand);
        giveCard(myHand);
        giveCard(dealersHand);
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if (!(Deck.costOf(myHand) > BLACK_JACK || deck.isEmpty())) {
            giveCard(myHand);
        }
        return (Deck.costOf(myHand) > 21) ? false : true;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (Deck.costOf(dealersHand) < DEALER_MAX) {
            giveCard(dealersHand);
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        if (bust(myHand)) {
            return WinState.LOOSE;
        } else if (bust(dealersHand)) {
            return WinState.WIN;
        } else if (Deck.costOf(myHand) == Deck.costOf(dealersHand)) {
            return WinState.PUSH;
        }
        return Deck.costOf(myHand) > Deck.costOf(dealersHand) ? WinState.WIN : WinState.LOOSE;
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return myHand;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return dealersHand;
    }

    private void reset() {
        myHand.clear();
        dealersHand.clear();
    }

    private void giveCard(List<Card> hand) {
        Card card = deck.get(0);
        hand.add(card);
        deck.remove(card);
    }

    private boolean bust(List<Card> hand) {
        return Deck.costOf(hand) > BLACK_JACK;
    }
}
