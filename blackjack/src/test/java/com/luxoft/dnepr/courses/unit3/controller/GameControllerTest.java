package com.luxoft.dnepr.courses.unit3.controller;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import static org.junit.Assert.*;

public class GameControllerTest {

    @Before
    // ���������� ����, ������� ������� ���������� ��� ������� ����� (����� ��� �����������)
    public void prepare() throws Exception {
        Field fld = GameController.class.getDeclaredField("controller");
        try {
            fld.setAccessible(true);
            fld.set(null, null);
        } finally {
            fld.setAccessible(false);
        }
    }

    @Test
    public void testGetInstance() {
        assertSame(GameController.getInstance(), GameController.getInstance());
    }

    @Test
    public void testBeforeNewGame() {
        assertTrue(GameController.getInstance().getMyHand().isEmpty());
        assertTrue(GameController.getInstance().getDealersHand().isEmpty());

        assertEquals(WinState.PUSH, GameController.getInstance().getWinState());
    }

    @Test
    public void testAfterNewGame() {
        GameController.getInstance().newGame();
        assertEquals(2, GameController.getInstance().getMyHand().size());
        assertEquals(1, GameController.getInstance().getDealersHand().size());
    }

    @Test
    public void testRequestMore() {
        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_10, Suit.SPADES));
                deck.set(1, new Card(Rank.RANK_ACE, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_JACK, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_2, Suit.HEARTS));
                deck.set(4, new Card(Rank.RANK_3, Suit.HEARTS));
                deck.set(5, new Card(Rank.RANK_QUEEN, Suit.HEARTS));
                deck.set(6, new Card(Rank.RANK_8, Suit.HEARTS));
            }

        });
        assertEquals(GameController.getInstance().getMyHand().size(), 2);
        assertEquals(Rank.RANK_10, GameController.getInstance().getMyHand().get(0).getRank());
        assertEquals(Rank.RANK_ACE, GameController.getInstance().getMyHand().get(1).getRank());

        assertEquals(1, GameController.getInstance().getDealersHand().size());
        assertEquals(Rank.RANK_JACK, GameController.getInstance().getDealersHand().get(0).getRank());

        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        GameController.getInstance().requestMore();
        assertEquals(Rank.RANK_2, GameController.getInstance().getMyHand().get(2).getRank());

        assertEquals(WinState.LOOSE, GameController.getInstance().getWinState());
        assertEquals(false, GameController.getInstance().requestMore());

        GameController.getInstance().getMyHand().remove(1);
        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

    }

    @Test
    public void testRequestStop() {
        GameController.getInstance().newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                deck.set(0, new Card(Rank.RANK_ACE, Suit.SPADES));
                deck.set(1, new Card(Rank.RANK_9, Suit.SPADES));
                deck.set(2, new Card(Rank.RANK_2, Suit.SPADES));
                deck.set(3, new Card(Rank.RANK_2, Suit.HEARTS));
                deck.set(4, new Card(Rank.RANK_7, Suit.HEARTS));
                deck.set(5, new Card(Rank.RANK_6, Suit.HEARTS));
                deck.set(6, new Card(Rank.RANK_QUEEN, Suit.HEARTS));
                deck.set(7, new Card(Rank.RANK_ACE, Suit.HEARTS));
                deck.set(8, new Card(Rank.RANK_3, Suit.HEARTS));
                deck.set(9, new Card(Rank.RANK_6, Suit.HEARTS));
            }
        });
        GameController.getInstance().requestStop();
        List<Card> dealersHand = GameController.getInstance().getDealersHand();
        assertEquals(4, dealersHand.size());
        assertEquals(Rank.RANK_6, dealersHand.get(3).getRank());
        assertFalse(Deck.costOf(dealersHand) < 17);
        assertEquals(17, Deck.costOf(dealersHand));
        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        dealersHand.remove(3);

        GameController.getInstance().requestStop();
        assertEquals(4, dealersHand.size());
        assertFalse(Deck.costOf(dealersHand) < 17);
        assertEquals(21, Deck.costOf(dealersHand));
        assertEquals(Rank.RANK_QUEEN, dealersHand.get(3).getRank());
        assertEquals(WinState.LOOSE, GameController.getInstance().getWinState());

        dealersHand.remove(3);

        GameController.getInstance().requestStop();
        assertEquals(4, dealersHand.size());
        assertFalse(Deck.costOf(dealersHand) < 17);
        assertEquals(22, Deck.costOf(dealersHand));
        assertEquals(Rank.RANK_ACE, dealersHand.get(3).getRank());
        assertEquals(WinState.WIN, GameController.getInstance().getWinState());

        dealersHand.remove(3);

        GameController.getInstance().requestStop();
        assertEquals(5, dealersHand.size());
        assertFalse(Deck.costOf(dealersHand) < 17);
        assertEquals(20, Deck.costOf(dealersHand));
        assertEquals(Rank.RANK_6, dealersHand.get(4).getRank());
        assertEquals(WinState.PUSH, GameController.getInstance().getWinState());
    }
}
