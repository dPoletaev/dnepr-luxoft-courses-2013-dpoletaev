package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users = new HashMap<>();


    public Bank(String expectedJavaVersion) {
        String actualJavaVersion = System.getProperty("java.version");
        if (!actualJavaVersion.equals(expectedJavaVersion)) {
            String errorMessage = String.format("Java version error. Expected %s but actual is %s",
                    expectedJavaVersion, actualJavaVersion);
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, errorMessage);
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        if(amount == null){
            amount = BigDecimal.ZERO;
        }
        UserInterface fromUser = getUser(fromUserId);
        UserInterface toUser = getUser(toUserId);
        WalletInterface from = getUserWallet(fromUser, amount, "withdraw");
        WalletInterface to = getUserWallet(toUser, amount, "transfer");
        from.withdraw(amount);
        to.transfer(amount);
    }

    private UserInterface getUser(Long userId) throws NoUserFoundException {
        UserInterface user = users.get(userId);
        if (user == null) {
            String message = String.format("User with id %s not found", userId);
            throw new NoUserFoundException(userId, message);
        }
        return user;
    }

    private WalletInterface getUserWallet(UserInterface user, BigDecimal amount, String action) throws TransactionException {
        WalletInterface wallet = user.getWallet();
        try {
            checkWallet(wallet, action, amount);
        } catch (Exception ex) {
            String message = String.format("User '%s' %s", user.getName(), ex.getMessage());
            throw new TransactionException(message);
        }
        return wallet;
    }

    private void checkWallet(WalletInterface wallet, String action, BigDecimal amount) throws WalletIsBlockedException, InsufficientWalletAmountException, LimitExceededException {
        if (action.equals("withdraw")) {
            wallet.checkWithdrawal(amount);
        } else if (action.equals("transfer")) {
            wallet.checkTransfer(amount);
        }
    }


}
