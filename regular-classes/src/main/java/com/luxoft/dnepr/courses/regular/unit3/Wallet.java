package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet() {
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (amountToWithdraw == null) {
            amountToWithdraw = BigDecimal.ZERO;
        }
        checkAmountForNull();
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, "wallet is blocked");
        } else if (amountToWithdraw.compareTo(amount) > 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount,
                    String.format(Locale.US, "has insufficient funds (%.2f < %.2f)", amountToWithdraw, amount));
        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        setAmount(amount.subtract(amountToWithdraw));
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (amountToTransfer == null) {
            amountToTransfer = BigDecimal.ZERO;
        }
        checkAmountForNull();
        BigDecimal sum = amount.add(amountToTransfer);
        if (sum.compareTo(maxAmount) > 0) {
            String message = String.format(Locale.US, "wallet limit exceeded (%.2f + %.2f > %.2f)", amount, amountToTransfer, maxAmount);
            throw new LimitExceededException(id, amountToTransfer, amount, message);
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }

    private void checkAmountForNull() {
        if (amount == null) {
            amount = BigDecimal.ZERO;
        }
    }
}
