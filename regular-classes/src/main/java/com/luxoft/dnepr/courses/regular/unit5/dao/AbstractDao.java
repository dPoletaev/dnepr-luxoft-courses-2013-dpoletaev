package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public abstract class AbstractDao<T extends Entity> implements IDao<T> {

    private EntityStorage<T> storage;

    public void setStorage(EntityStorage<T> storage) {
        this.storage = storage;
    }

    public EntityStorage<T> getStorage() {
        return storage;
    }

    public T save(T entity) throws UserAlreadyExist {
        if (entity == null) {
            return null;
        } else if (existsId(entity.getId())) {
            throw new UserAlreadyExist(entity.getId());
        } else if (entity.getId() == null) {
            Long id = storage.getMaxId() + 1;
            entity.setId(id);
        }
        storage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    public T update(T entity) throws UserNotFound {
        if (entity == null) {
            throw new UserNotFound(null);
        }
        Long id = entity.getId();
        if (id == null || !existsId(id)) {
            throw new UserNotFound(id);
        }
        storage.getEntities().put(id, entity);
        return entity;
    }

    public T get(long id) {
        if (!existsId(id)) {
            return null;
        }
        return storage.getEntityById(id);
    }

    public boolean delete(long id) {
        if (!existsId(id)) {
            return false;
        }
        storage.getEntities().remove(id);
        return true;
    }

    private boolean existsId(Long id) {
        return storage.getEntities().containsKey(id);
    }
}
