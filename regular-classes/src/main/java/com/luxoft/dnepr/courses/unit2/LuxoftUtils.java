package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public class LuxoftUtils {

    private LuxoftUtils() {
    }

    public static String[] sort(String[] array, boolean asc) {
        return mergeSort(array.clone(), asc);
    }


    public static double wordAverageLength(String input) {
        if (input.isEmpty()) {
            return 0;
        }
        char[] chars = input.toCharArray();
        int totalWordsLength = chars.length;
        int wordsCount = 0;
        int startIndex;
        int endIndex;
        for (int i = 0; i < chars.length; ) {
            if (chars[i] == ' ') {
                totalWordsLength--;
                i++;
            } else {
                startIndex = i;
                endIndex = findEndIndex(chars, startIndex);
                wordsCount++;
                i = endIndex + 1;
            }
        }
        return (double) totalWordsLength / wordsCount;
    }

    public static String reverseWords(String input) {
        char[] chars = input.toCharArray();
        char[] result = new char[chars.length];
        ;
        int startIndex;
        int endIndex;
        for (int i = 0; i < chars.length; ) {
            if (chars[i] == ' ') {
                result[i] = chars[i];
                i++;
            } else {
                startIndex = i;
                endIndex = findEndIndex(chars, startIndex);
                insertReversedSequence(chars, result, startIndex, endIndex);
                i = endIndex + 1;
            }
        }
        return new String(result);
    }

    public static char[] getCharEntries(String str) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        char[] chars = str.toCharArray();
        for (char ch : chars) {
            if (ch != 32) {
                if (map.containsKey(ch)) {
                    int value = map.get(ch);
                    map.put(ch, ++value);
                } else {
                    map.put(ch, 1);
                }
            }
        }
        List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character, Integer>>(map.entrySet());
        Collections.sort(list, new EntryComparator());
        char[] result = new char[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i).getKey();
        }
        return result;
    }

    public static double calculateOverallArea(List<Figure> figures) {
        double result = 0;
        for (Figure figure : figures) {
            result += figure.calculateArea();
        }
        return result;
    }

    private static String[] mergeSort(String[] array, boolean asc) {
        if (array.length <= 1) {
            return array;
        }
        int center = array.length / 2;
        String[] left = new String[center];
        String[] right = new String[array.length - center];
        int rightIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (i < center) {
                left[i] = array[i];
            } else {
                right[rightIndex++] = array[i];
            }
        }
        return merge(mergeSort(left, asc), mergeSort(right, asc), asc);
    }

    private static String[] merge(String[] left, String[] right, boolean asc) {
        int totalLength = left.length + right.length;
        String[] merged = new String[totalLength];
        int leftIndex = 0;
        int rightIndex = 0;
        for (int i = 0; i < totalLength; i++) {
            if (leftIndex < left.length && rightIndex < right.length) {
                merged[i] = needsReplace(left[leftIndex], right[rightIndex], asc) ? right[rightIndex++] : left[leftIndex++];
            } else {
                merged[i] = leftIndex < left.length ? left[leftIndex++] : right[rightIndex++];
            }
        }
        return merged;
    }

    private static boolean needsReplace(String first, String second, boolean asc) {
        return asc ? first.compareTo(second) > 0 : first.compareTo(second) < 0;
    }


    private static int findEndIndex(char[] chars, int startIndex) {
        for (int i = startIndex; i < chars.length; i++) {
            if (chars[i] == ' ') {
                return i - 1;
            }
        }
        return chars.length - 1;
    }

    private static void insertReversedSequence(char[] chars, char[] result, int start, int end) {
        int sequenceLength = end - start + 1;
        for (int i = 0; i < sequenceLength; i++) {
            result[start + i] = chars[end - i];
        }
    }


    static class EntryComparator implements Comparator<Map.Entry<Character, Integer>> {
        @Override
        public int compare(Map.Entry<Character, Integer> firstEntry, Map.Entry<Character, Integer> secondEntry) {
            Character firstKey = firstEntry.getKey();
            Character secondKey = secondEntry.getKey();
            int firstValue = firstEntry.getValue();
            int secondValue = secondEntry.getValue();
            int valueDifference = firstValue - secondValue;
            return valueDifference != 0 ? -valueDifference : firstKey.compareTo(secondKey);
        }
    }
}
