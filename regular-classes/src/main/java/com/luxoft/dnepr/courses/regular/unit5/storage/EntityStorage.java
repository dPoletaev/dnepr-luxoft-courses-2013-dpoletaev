package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EntityStorage<T extends Entity> {
    private final Map<Long, T> entities = new HashMap();


    public Map<Long, T> getEntities() {
        return entities;
    }

    public T getEntityById(Long id) {
        return entities.get(id);
    }

    public Long getMaxId() {
        Long maxId = new Long(0);
        Set<Long> set = entities.keySet();
        for (Long id : set) {
            if (id.compareTo(maxId) > 0) {
                maxId = id;
            }
        }
        return maxId;
    }

}
