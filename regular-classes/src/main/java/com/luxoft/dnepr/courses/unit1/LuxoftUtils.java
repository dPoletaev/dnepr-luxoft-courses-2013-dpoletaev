package com.luxoft.dnepr.courses.unit1;


public class LuxoftUtils {

    private LuxoftUtils() {
    }


    public static String getMonthName(int monthOrder, String language) {

        if (language.equals("en")) {
            return getMonthInEnglish(monthOrder);
        } else if (language.equals("ru")) {
            return getMonthInRussian(monthOrder);
        } else {
            return "Unknown Language";
        }
    }

    private static String getMonthInEnglish(int month) {
        switch (month) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "Unknown Month";
        }
    }

    private static String getMonthInRussian(int month) {
        switch (month) {
            case 1:
                return "Январь";
            case 2:
                return "Февраль";
            case 3:
                return "Март";
            case 4:
                return "Апрель";
            case 5:
                return "Май";
            case 6:
                return "Июнь";
            case 7:
                return "Июль";
            case 8:
                return "Авгусь";
            case 9:
                return "Сентябрь";
            case 10:
                return "Октябрь";
            case 11:
                return "Ноябрь";
            case 12:
                return "Декабрь";
            default:
                return "Неизвестный месяц";
        }
    }


    public static String binaryToDecimal(String binaryNumber) {
        if (binaryNumber == null || binaryNumber.isEmpty()) {
            return "Not binary";
        }
        int result = 0;
        char[] binarySymbols = binaryNumber.toCharArray();
        int power = binarySymbols.length - 1;
        for (char binarySymbol : binarySymbols) {
            if (binarySymbol != '1' && binarySymbol != '0') {
                return "Not binary";
            }
            if (binarySymbol == '1') {
                result += Math.pow(2, power);
            }
            power--;
        }
        return String.valueOf(result);
    }

    public static String decimalToBinary(String decimalNumber) {
        StringBuffer result = new StringBuffer();
        try {
            int intDecimalNumber = Integer.parseInt(decimalNumber);
            while (intDecimalNumber > 0) {
                result.append(intDecimalNumber % 2);
                intDecimalNumber /= 2;
            }
        } catch (NumberFormatException ex) {
            return "Not decimal";
        }
        return result.reverse().toString();
    }

    public static int[] sortArray(int[] array, boolean asc) {
        boolean changed;
        int[] resultArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            resultArray[i] = array[i];
        }
        do {
            changed = false;
            for (int i = 0; i < resultArray.length - 1; i++) {
                if (check(resultArray[i], resultArray[i + 1], asc)) {
                    change(resultArray, i);
                    changed = true;
                }
            }
        } while (changed);

        return resultArray;
    }

    private static boolean check(int a, int b, boolean asc) {
        if (asc) {
            return a > b;
        }
        return a < b;
    }

    private static void change(int[] array, int index) {
        int temp = array[index];
        array[index] = array[index + 1];
        array[index + 1] = temp;
    }
}
