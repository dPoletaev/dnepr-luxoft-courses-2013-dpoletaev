package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

public class EqualSet<E> implements Set<E> {
    private List<E> list;

    public EqualSet() {
        list = new ArrayList<>();
    }

    public EqualSet(Collection<? extends E> collection) {
        this();
        addAll(collection);
    }


    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty() ? true : false;
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
//        for (Object obj : list) {
//            if(o == null){
//                if(obj == null){
//                    return true;
//                }
//            }
//            else{
//                if(o.equals(obj)){
//                    return true;
//                }
//            }
//        }
//        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return list.toArray(ts);
    }

    @Override
    public boolean add(E e) {
        if (list.contains(e)) {
            return false;
        }
        list.add(e);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (!contains(o)) {
            return false;
        }
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        for (Object obj : objects) {
            if (!contains(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> es) {
        boolean added = false;
        for (E obj : es) {
            if (!contains(obj)) {
                add(obj);
                added = true;
            }
        }
        return added;
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return list.retainAll(objects);
//        return removeOrRetain(objects, false);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return list.removeAll(objects);
//        return removeOrRetain(objects, true);
    }

    @Override
    public String toString() {
        return list.toString();
    }

    @Override
    public void clear() {
        list.clear();
    }


    private boolean removeOrRetain(Collection<?> objects, boolean remove) {
        boolean changed = false;
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Object obj = iterator.next();
            if (remove ? objects.contains(obj) : !objects.contains(obj)) {
                iterator.remove();
                changed = true;
            }
        }
        return changed;
    }

}

