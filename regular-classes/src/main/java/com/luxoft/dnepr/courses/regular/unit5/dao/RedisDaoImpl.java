package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.RedisStorage;

public class RedisDaoImpl extends AbstractDao<Redis> {

    public RedisDaoImpl() {
        setStorage(RedisStorage.getInstance());
    }
}
