package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Calendar;
import java.util.Date;

/**
 * Product factory.
 * Simplifies creation of different kinds of products.
 */
public class ProductFactory {

    public Bread createBread(String code, String name, double price, double weight) {
        Bread bread = new Bread();
        bread.setName(name);
        bread.setPrice(price);
        bread.setWeight(weight);
        bread.setCode(code);
        return bread;
    }

    public Beverage createBeverage(String code, String name, double price, boolean nonAlcoholic) {
        Beverage beverage = new Beverage();
        beverage.setNonAlcoholic(nonAlcoholic);
        beverage.setCode(code);
        beverage.setName(name);
        beverage.setPrice(price);
        return beverage;
    }

    public Book createBook(String code, String name, double price, Date publicationDate) {
        Book book = new Book();
        book.setPublicationDate(publicationDate);
        book.setPrice(price);
        book.setCode(code);
        book.setName(name);
        return book;
    }
}
