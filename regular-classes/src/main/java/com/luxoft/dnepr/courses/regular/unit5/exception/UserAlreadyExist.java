package com.luxoft.dnepr.courses.regular.unit5.exception;

public class UserAlreadyExist extends Exception {
    public UserAlreadyExist(Long id) {
        super(String.format("User with id %s already exists in storage", id));
    }
}
