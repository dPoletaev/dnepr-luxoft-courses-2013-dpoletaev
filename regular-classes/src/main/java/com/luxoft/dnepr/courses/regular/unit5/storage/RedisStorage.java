package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

public class RedisStorage extends EntityStorage<Redis> {
    private static final RedisStorage INSTANCE = new RedisStorage();

    private RedisStorage() {

    }

    public static RedisStorage getInstance() {
        return INSTANCE;
    }
}
