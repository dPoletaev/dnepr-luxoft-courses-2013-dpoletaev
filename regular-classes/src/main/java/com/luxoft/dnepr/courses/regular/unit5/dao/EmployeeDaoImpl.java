package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.storage.EmployeeStorage;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public class EmployeeDaoImpl extends AbstractDao<Employee> {


    public EmployeeDaoImpl() {
        setStorage(EmployeeStorage.getInstance());
    }


}
