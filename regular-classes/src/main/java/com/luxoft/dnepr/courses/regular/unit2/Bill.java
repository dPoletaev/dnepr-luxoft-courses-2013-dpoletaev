package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private Map<Product, CompositeProduct> products = new HashMap<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        if (products.get(product) == null) {
            CompositeProduct compositeProduct = new CompositeProduct();
            compositeProduct.add(product);
            products.put(product, compositeProduct);
        } else {
            products.get(product).add(product);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double sum = 0;
        for (CompositeProduct compositeProduct : products.values()) {
            sum += compositeProduct.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> list = new ArrayList<>();
        for (Product product : products.values()) {
            list.add(product);
        }
        Collections.sort(list, new ProductComparator());
        return list;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    static class ProductComparator implements Comparator<Product> {

        @Override
        public int compare(Product product, Product product2) {
            double difference = product.getPrice() - product2.getPrice();
            if (difference == 0) {
                return 0;
            }
            return difference > 0 ? -1 : 1;
        }
    }

}
