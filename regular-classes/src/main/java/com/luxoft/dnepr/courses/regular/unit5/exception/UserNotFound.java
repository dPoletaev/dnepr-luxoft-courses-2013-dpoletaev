package com.luxoft.dnepr.courses.regular.unit5.exception;

public class UserNotFound extends Exception {

    public UserNotFound(Long id) {
        super(id == null ? "User without id can't be found" : String.format("User with id %s not found", id));
    }
}
