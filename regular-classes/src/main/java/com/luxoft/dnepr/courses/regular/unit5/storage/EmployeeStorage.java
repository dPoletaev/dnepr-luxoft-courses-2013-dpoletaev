package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;

public class EmployeeStorage extends EntityStorage<Employee> {

    private static final EmployeeStorage INSTANCE = new EmployeeStorage();

    private EmployeeStorage() {
    }

    public static EmployeeStorage getInstance() {
        return INSTANCE;
    }

}
