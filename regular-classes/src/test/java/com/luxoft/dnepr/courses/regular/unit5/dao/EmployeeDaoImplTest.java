package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EmployeeStorage;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import java.util.Map;

public class EmployeeDaoImplTest {
    private EmployeeDaoImpl dao = new EmployeeDaoImpl();
    Map<Long, Employee> map = EmployeeStorage.getInstance().getEntities();


    public void setEntity() {
        dao.setStorage(EmployeeStorage.getInstance());
    }

    @After
    public void tearDown() throws Exception {
        map.clear();
    }

    @Test
    public void testSave() throws Exception {
        assertEquals(new Employee(new Long(1), 1500), dao.save(new Employee(1500)));
        assertEquals(new Employee(new Long(1), 1500), map.get(new Long(1)));
        dao.save(new Employee(new Long(999), 2000));
        assertEquals(new Employee(new Long(1000), 2300), dao.save(new Employee(2300)));
        assertEquals(new Employee(new Long(1000), 2300), map.get(new Long(1000)));
        assertEquals(3, map.size());
        try {
            dao.save(new Employee(new Long(1000), 2000));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserAlreadyExist);
        }
        assertNull(dao.save(null));
    }

    @Test
    public void testUpdate() throws Exception {
        Employee emp = new Employee(new Long(10), 1400);
        dao.save(emp);
        Employee emp2 = new Employee(new Long(10), 1500);
        assertEquals(new Employee(new Long(10), 1500), dao.update(emp2));
        assertEquals(emp2, map.get(new Long(10)));
        try {
            dao.update(new Employee(new Long(1), 1000));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserNotFound);
        }
        try {
            dao.update(new Employee(2000));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserNotFound);
        }
        try {
            dao.update(null);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserNotFound);
        }
    }

    @Test
    public void testGet() throws Exception {
        Employee emp = new Employee(new Long(140), 1500);
        dao.save(emp);
        assertEquals(emp, dao.get(new Long(140)));
        assertNull(dao.get(141));
    }

    @Test
    public void testDelete() throws Exception {
        dao.save(new Employee(new Long(20), 1000));
        assertTrue(dao.delete(20));
        assertNull(dao.get(20));
        assertFalse(dao.delete(20));
    }
}
