package com.luxoft.dnepr.courses.regular.unit4;

import static org.junit.Assert.*;

import org.junit.*;

import java.util.*;

public class EqualSetTest {
    private Set<String> set;

    @Before
    public void setUp() {
        set = new EqualSet<>();
    }

    @Test
    public void testSize() throws Exception {
        List<String> list = new ArrayList<>();
        list.add("first");
        list.add("second");
        list.add("third");
        set = new EqualSet<>(list);
        assertEquals(3, set.size());
    }


    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(set.isEmpty());
        set.add("aaa");
        assertFalse(set.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        set.add("aaa");
        assertTrue(set.contains("aaa"));
        assertFalse(set.contains("bbb"));
        assertFalse(set.contains(null));
        set.add(null);
        assertTrue(set.contains(null));
    }

    @Test
    public void testIterator() throws Exception {
        Iterator<String> it = set.iterator();
        assertNotNull(it);
    }

    @Test
    public void testToArray() throws Exception {
        String[] expectedArray = {"a", "b", "c"};
        set = new EqualSet<>(Arrays.asList(expectedArray));
        assertArrayEquals(expectedArray, set.toArray());
        String[] array2 = new String[0];
        assertArrayEquals(expectedArray, set.toArray(array2));
        String[] array3 = new String[5];
        String[] resultArray = {"a", "b", "c", null, null};
        assertArrayEquals(resultArray, set.toArray(array3));
        assertSame(array3, set.toArray(array3));
        array3 = new String[2];
        String[] actualArray = set.toArray(array3);
        assertNotSame(array3, actualArray);
        assertArrayEquals(expectedArray, actualArray);
        array3 = null;
        try {
            set.toArray(array3);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof NullPointerException);
        }
    }


    @Test
    public void testAdd() throws Exception {
        assertTrue(set.add("first"));
        assertTrue(set.add(null));
        assertFalse(set.add("first"));
        assertFalse(set.add(null));
        assertEquals(2, set.size());
    }

    @Test
    public void testRemove() throws Exception {
        set.add("aaa");
        set.add("bbb");
        assertTrue(set.remove("aaa"));
        assertEquals(1, set.size());
        assertFalse(set.remove("aaa"));
        assertEquals(1, set.size());
    }

    @Test
    public void testContainsAll() throws Exception {
        set.add("aaa");
        set.add("bbb");
        set.add("ccc");
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        assertTrue(set.containsAll(list));
        list.remove("ccc");
        assertTrue(set.containsAll(list));
        set.remove("aaa");
        assertFalse(set.containsAll(list));
    }

    @Test
    public void testAddAll() throws Exception {
        List<String> list = new ArrayList<>();
        list.add("aa");
        list.add("bb");
        assertTrue(set.addAll(list));
        assertEquals(2, set.size());
        list.add("ccc");
        assertTrue(set.addAll(list));
        assertEquals(3, list.size());
        assertFalse(set.addAll(list));
        list = null;
        try {
            set.addAll(list);
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof NullPointerException);
        }

    }

    @Test
    public void testRetainAll() throws Exception {
        set = new EqualSet<>();
        set.add("aaa");
        set.add("bbb");
        set.add("ccc");
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("ccc");
        list.add("ddd");
        assertTrue(set.retainAll(list));
        assertEquals(2, set.size());
        assertFalse(set.contains("bbb"));
        assertFalse(set.retainAll(list));
        assertEquals(2, set.size());
    }

    @Test
    public void testRemoveAll() throws Exception {
        set = new EqualSet<>();
        set.add("aaa");
        set.add("bbb");
        set.add("ccc");
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("ccc");
        list.add("ddd");
        assertTrue(set.removeAll(list));
        assertEquals(1, set.size());
        assertTrue(set.contains("bbb"));
        assertFalse(set.removeAll(list));
        assertEquals(1, set.size());
    }

    @Test
    public void testClear() throws Exception {
        set = new EqualSet<>();
        set.add("aaa");
        set.add("bbb");
        set.add("ccc");
        set.clear();
        assertEquals(0, set.size());
    }
}
