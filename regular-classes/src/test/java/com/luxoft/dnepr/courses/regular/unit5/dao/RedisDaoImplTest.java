package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.RedisStorage;
import org.junit.After;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class RedisDaoImplTest {
    private RedisDaoImpl dao = new RedisDaoImpl();
    Map<Long, Redis> map = RedisStorage.getInstance().getEntities();

    @After
    public void tearDown() throws Exception {
        map.clear();
    }

    @Test
    public void testSave() throws Exception {
        assertEquals(new Redis(new Long(1), 1500), dao.save(new Redis(1500)));
        assertEquals(new Redis(new Long(1), 1500), map.get(new Long(1)));
        dao.save(new Redis(new Long(999), 2000));
        assertEquals(new Redis(new Long(1000), 2300), dao.save(new Redis(2300)));
        assertEquals(new Redis(new Long(1000), 2300), map.get(new Long(1000)));
        assertEquals(3, map.size());
        try {
            dao.save(new Redis(new Long(1000), 2000));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserAlreadyExist);
        }
    }

    @Test
    public void testUpdate() throws Exception {
        Redis redis = new Redis(new Long(10), 1400);
        dao.save(redis);
        Redis redis2 = new Redis(new Long(10), 1500);
        assertEquals(new Redis(new Long(10), 1500), dao.update(redis2));
        assertEquals(redis2, map.get(new Long(10)));
        try {
            dao.update(new Redis(new Long(1), 1000));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserNotFound);
        }
        try {
            dao.update(new Redis(2000));
            fail();
        } catch (Exception ex) {
            assertTrue(ex instanceof UserNotFound);
        }
    }

    @Test
    public void testGet() throws Exception {
        Redis redis = new Redis(new Long(140), 1500);
        dao.save(redis);
        assertEquals(redis, dao.get(new Long(140)));
        assertNull(dao.get(141));
    }

    @Test
    public void testDelete() throws Exception {
        dao.save(new Redis(new Long(20), 1000));
        assertTrue(dao.delete(20));
        assertNull(dao.get(20));
        assertFalse(dao.delete(20));
    }
}
