package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        Queue inputInPolish = Translator.translateInReversePolish(input);
        processInput(result, inputInPolish);
        return result.toByteArray();
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

    private static void processInput(ByteArrayOutputStream result, Queue input) {
        while (!input.isEmpty()) {
            Object expressionPart = input.poll();
            if (expressionPart instanceof Double) {
                addCommand(result, VirtualMachine.PUSH, (Double) expressionPart);
            } else if (expressionPart instanceof Character) {
                processOperation(result, (Character) expressionPart);
            }
        }
        addCommand(result, VirtualMachine.PRINT);
    }

    private static void processOperation(ByteArrayOutputStream result, char operation) {
        if (operation == '/' || operation == '-') {
            addCommand(result, VirtualMachine.SWAP);
        }
        addCommand(result, getOperationInBytes(operation));
    }

    private static byte getOperationInBytes(char c) {
        switch (c) {
            case '-':
                return VirtualMachine.SUB;
            case '*':
                return VirtualMachine.MUL;
            case '/':
                return VirtualMachine.DIV;
            default:
                return VirtualMachine.ADD;
        }
    }
}


