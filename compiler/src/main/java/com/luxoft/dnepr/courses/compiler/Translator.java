package com.luxoft.dnepr.courses.compiler;

import java.util.*;

public class Translator {
    private final static char[] signs = {'+', '-', '*', '/', '(', ')'};
    private final static char[] operationSigns = {'+', '-', '*', '/'};

    private Translator() {
    }

    public static Queue translateInReversePolish(String input) {
        List expressionList = parseExpression(input);
        Queue output = new LinkedList();
        Stack stack = new Stack();
        for (Object obj : expressionList) {
            readExpressionObject(obj, output, stack);
        }
        emptyStack(output, stack);
        return output;
    }

    private static void readExpressionObject(Object obj, Queue output, Stack stack) {
        if (obj instanceof Double) {
            output.add(obj);
        }
        if (obj instanceof Character) {
            char sign = (Character) obj;
            if (isOperationSign(sign)) {
                processOperationSign(sign, output, stack);
            } else if (sign == '(') {
                stack.push(sign);
            } else if (sign == ')') {
                findOpenBracket(output, stack);
            }
        }
    }

    private static void processOperationSign(char sign, Queue output, Stack stack) {
        while (!stack.isEmpty()) {
            char signInStack = (Character) stack.peek();
            if (getPriority(signInStack) < getPriority(sign)) {
                break;
            } else {
                output.add(stack.pop());
            }
        }
        stack.push(sign);
    }

    private static void emptyStack(Queue output, Stack stack) {
        while (!stack.isEmpty()) {
            if (stack.peek().equals('(')) {
                throw new CompilationException("Incorrect expression");
            }
            output.add(stack.pop());
        }
    }

    private static void findOpenBracket(Queue output, Stack stack) {
        while (true) {
            Object obj = stack.pop();
            if (obj.equals('(')) {
                break;
            }
            output.add(obj);
            if (stack.isEmpty()) {
                throw new CompilationException("Incorrect expression");
            }
        }
    }

    private static int getPriority(char sign) {
        if (sign == '*' || sign == '/') {
            return 2;
        } else if (sign == '+' || sign == '-') {
            return 1;
        } else {
            return 0;
        }
    }


    private static List parseExpression(String expression) {
        List resultList = new ArrayList();
        char[] chars = removeSpaces(expression).toCharArray();
        for (int i = 0; i < chars.length; ) {
            if (Character.isDigit(chars[i])) {
                int endIndex = findNumberEndIndex(chars, i);
                resultList.add(readDouble(chars, i, endIndex));
                i = endIndex + 1;
            } else {
                resultList.add(chars[i]);
                i++;
            }
        }
        return resultList;
    }

    private static double readDouble(char[] chars, int start, int end) {
        StringBuilder number = new StringBuilder();
        for (int i = start; i <= end; i++) {
            number.append(chars[i]);
        }
        return Double.parseDouble(number.toString());
    }

    private static String removeSpaces(String input) {
        StringBuilder result = new StringBuilder();
        char[] chars = input.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            checkChar(chars[i]);
            if (chars[i] == ' ') {
                checkSpace(i, chars);
            } else {
                result.append(chars[i]);
            }
        }
        return result.toString();
    }

    private static void checkSpace(int index, char[] chars) {
        if (!(index == 0 || index == chars.length - 1)) {
            int nonSpaceIndex = findNonSpaceIndex(index, chars);
            if (nonSpaceIndex > 0) {
                if (Character.isDigit(chars[index - 1]) && Character.isDigit(chars[nonSpaceIndex])) {
                    throw new CompilationException("Illegal expression. Space between digits");
                }
            }
        }
    }

    private static int findNonSpaceIndex(int start, char[] chars) {
        for (int i = start + 1; i < chars.length; i++) {
            if (chars[i] != ' ') {
                return i;
            }
        }
        return -1;
    }

    private static void checkChar(char c) {
        if (!(Character.isDigit(c) || c == '.' || c == ' ' || isSign(c))) {
            throw new CompilationException("Expression contains illegal sign: " + c);
        }
    }

    private static int findNumberEndIndex(char[] chars, int start) {
        for (int i = start + 1; i < chars.length; i++) {
            if (!(Character.isDigit(chars[i]) || chars[i] == '.')) {
                return i - 1;
            }
        }
        return chars.length - 1;
    }

    private static boolean isSign(char c) {
        for (char sign : signs) {
            if (sign == c) {
                return true;
            }
        }
        return false;
    }

    private static boolean isOperationSign(char c) {
        for (char sign : operationSigns) {
            if (sign == c) {
                return true;
            }
        }
        return false;
    }
}
