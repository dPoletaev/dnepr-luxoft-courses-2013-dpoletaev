package com.luxoft.dnepr.courses.compiler;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class TranslatorTest {
    @Test
    public void testTranslateIntoPolish() throws Exception {
        Queue expected = new LinkedList();

        Object[] expectedArray = {3.0, 4.0, 2.0, '*', 1.0, 5.0, '-', '/', '+'};
        expected.addAll(Arrays.asList(expectedArray));
        testResult(expected, Translator.translateInReversePolish("3+4*2/(1 -5) "));

        Object[] expectedArray2 = {1.5, 2.75, '+'};
        expected.clear();
        expected.addAll(Arrays.asList(expectedArray2));
        testResult(expected, Translator.translateInReversePolish("1.5 + 2.75"));

        Object[] expectedArray3 = {3.5, 10.0, 2.0, '-', '*', 25.0, 5.0, '/', '/'};
        expected.clear();
        expected.addAll(Arrays.asList(expectedArray3));
        testResult(expected, Translator.translateInReversePolish("3.5 * (10 - 2) / (25 / 5)"));
    }

    @Test
    public void testExpectedExceptions() {
        testIncorrectInput("2+2*(10-5");
        testIncorrectInput("2+2*10-5)");
        testIncorrectInput("(2+2)*10-5)");
        testIncorrectInput("4 8 / 5");
        testIncorrectInput("45 / 5f");
    }

    private void testIncorrectInput(String input) {
        try {
            Translator.translateInReversePolish(input);
            fail();
        } catch (CompilationException ex) {
        }
    }


    private void testResult(Queue expected, Queue result) {
        while (!result.isEmpty()) {
            Object expectedObj = expected.poll();
            Object resultObj = result.poll();
            assertEquals(expectedObj, resultObj);
        }
    }

}
